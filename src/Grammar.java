////////////////////////////////////////////////////////////////////////////////
//
//      MP 4 - cs585
//
//      Paul Chase
//
//      This implements a cfg style grammar
////////////////////////////////////////////////////////////////////////////////

import java.io.*;
import java.text.*;
import java.util.*;
import java.util.regex.*;
import java.lang.Math.*;

//this class implements the third assignment for CS585
public class Grammar
{
	private Vector<Production> productions;
	private Vector<String> nonTerminals;


	//this reads the grammar in from a file
	public Grammar(String f) throws Exception
	{
		productions = new Vector<Production>();
		productions.clear();
		nonTerminals = new Vector<String>();
		nonTerminals.clear();

		//load the file
		BufferedReader br = new BufferedReader(new FileReader(f));
		Production p;
		String str = br.readLine();
		String rule[];
		while(str!=null)
		{
			rule = str.split("\t");
			p = new Production();
			//p.probability = (new Float(rule[0])).floatValue();
            p.probability = Float.parseFloat(rule[0]);
			p.left = rule[1];
			p.right = rule[2].split(" ");
			p.dot = 0;
			p.start = 0;
			productions.add(p);
			addNonTerminal(rule[1]);
			str = br.readLine();
		}
		br.close();
	}

	//checks if we've seen this nonterminal; if we haven't, add it
	private void addNonTerminal(String s)
	{
		for(int i=0;i<nonTerminals.size();i++)
		{
			if((nonTerminals.get(i)).compareTo(s)==0)
				return;
		}
		nonTerminals.add(s);
	}

	private final boolean isNonTerminal(String s)
	{
		//return true if it's a non-terminal
		for(int i=0;i<nonTerminals.size();i++)
		{
			if((nonTerminals.get(i)).compareTo(s)==0)
				return true;
		}
		return false;
	}

	//this function predicts possible completions of p, and adds them to v
	private final void predict(Vector<Production> v, Production p, int pos)
	{
		Vector<Production> prods = getProds(p.right[p.dot]);
		Production q,r;
        //System.out.println("predict:"+p.toString());
		for(int j=0;j<prods.size();j++)
		{
			r = prods.get(j);
			q = new Production(r);
			q.dot = 0;
			q.start = pos;
			addProd(v,q);
		}
	}

	//this checks if we can scan s on the current production p
	private final boolean scan(Vector<Production> v, Production p, String s)
	{
		Production q;
		//System.out.println("scan:"+p.toString());
		if(p.right[p.dot].compareTo(s)==0)
		{
			//match - add it to the next vector
			q = new Production(p);
			q.dot = q.dot + 1;
            //scan makes hierarchy.
            q.children = null;//set the children to null since there is no further action after scan
            addProd(v, q);
			return true;
		}
		return false;
	}

	//this takes a completed production and tries to attach it back in the
	//cols table, putting any attachments into cur.
	private final void attach(Vector<Vector<Production>> cols, Vector<Production> cur, Production p)
	{
		//if the next thing in one rule is the first thing in this rule,
		//we attach.  otherwise ignore
		Vector<Production> col;
		Production q,r;
		String s = p.left;
		col = cols.get(p.start);
        int probFlag = 0;
        if(p.probability == 0.0){
            probFlag = 0;
        } else {
            probFlag = 1;
        }
		//System.out.println("attach:"+p.toString());
		for(int j=0;j<col.size();j++)
		{
            int flag = 0;
            q = col.get(j);
			if(q.right.length > q.dot){//Otherwise, visited!!
				if(q.right[q.dot].compareTo(s)==0)
				{	//Attach!
					r = new Production(q);
					r.dot = r.dot + 1;
                    if(r.children == null)//create children
                        r.children = new Production[1];
                    if(r.children[r.children.length-1] == null){//Directly append production as children
                        //System.out.println("Directly Append!");
                        r.children[r.children.length-1] = new Production(p);
                    } else {//if all the slot are taken, we will make more room for the coming children
                        //System.out.println("Copy Append!!!");
                        r.children = Arrays.copyOf(r.children, r.children.length+1);
                        r.children[r.children.length-1] = new Production(p);
                    }
                    if(probFlag == 1)//probability is implemented
                        r.probability = (float)Math.pow(2.0, Math.log(r.probability) + Math.log(p.probability));
                    if(p.parent == null){
                        p.parent = new Production(r);
                    } else {
                        p.parent = new Production(r);
                    }
                    //System.out.println("LENGTH = " + r.children.length);
                    //System.out.println(r.toString() + " --->>> " + p.toString());
                    //Merge previous visited prods
                    for(int w=0; w<cur.size(); w++){
                        if(cur.get(w).equals(r) && !(cur.get(w).children[r.dot-1].equals(r.children[r.dot-1]))){
                            //those two productions have the same left but different rights
                            if(probFlag == 0){
                                //Probability is not provided
                                //Creating parallel branches
                                cur.get(w).children[r.dot-1].nextProd = r.children[r.dot-1];
                            } else {
                                //Check the probability and pick the largest one
                                if(Math.log(cur.get(w).probability) < Math.log(r.probability)){
                                    cur.remove(w);
                                } else if(Math.log(cur.get(w).probability) == Math.log(r.probability)){
                                    //Even though probability is provided, what if we still have the same probability??
                                    cur.get(w).children[r.dot-1].nextProd = r.children[r.dot-1];
                                } else {
                                    flag = 1;
                                }
                            }
                        }
                    }
                    if(flag == 0)
                        addProd(cur,r);
                    flag = 0;
                }
            }
		}
	}

	//this parses the sentence
	public final Production parse(String sent[])
	{
		//this is a vector of vectors, storing the columns of the table
		Vector<Vector<Production>> cols = new Vector<Vector<Production>>();	cols.clear();
		//this is the current column; a vector of production indices
		Vector<Production> cur = new Vector<Production>();	cur.clear();
		//this is the next column; a vector of production indices
		Vector<Production> next = new Vector<Production>();	next.clear();
		//add the first symbol
		cur.add(getProds("ROOT").get(0));
		Production p;
		for(int pos=0;pos<=sent.length;pos++)
		{
			//System.out.println("Pos = " + pos);
			int i=0;
			boolean match = false;
			//check through the whole vector, even as it gets bigger
			while(i!=cur.size())
			{
				p = cur.get(i);
				if(p.right.length > p.dot)
				{	//predict and scan
					if(sent.length == pos)
					{
						match = true;
					} else{
						if(isNonTerminal(p.right[p.dot]))
						{
							//predict adds productions to cur
							predict(cur,p,pos);
						}else{
							//scan adds productions to next
						    //System.out.println("scan: " + p.toString() + " ("+pos+")= " + sent[pos]);
						    if(scan(next,p,sent[pos])) {
							    //System.out.println("  Found: " + sent[pos]);
							    match = true;
						    }
						}
					}
				} else {	//attach add productions to cur//dot at the end
				    attach(cols,cur,p);
				    if(sent.length == pos)
					{
					    match = true;
					}
				}
				i++;
				//when using a gargantuan grammar
				//this spits out stuff if it's taking a long time.
				if(i%100 == 0)
					System.out.print(".");
			}
			//System.out.println("Match = " + match);
			cols.add(cur);
            //
			if(!match)
			{
			    printTable(cols,sent);
			    System.out.println("Failed on: "+ cur);
			    return null;
			}
			cur = next;
			next = new Vector<Production>();	next.clear();
			System.out.println();
		}

		//print the Earley table
		//Comment this out once you've got parses printing; it's
		//only here for your evaluation.
		//printTable(cols,sent);

		//Right now we simply check to see if a parse exists;
		//in other words, we see if there's a "ROOT -> x x x ."
		//production in the last column.  If there is, it's returned; otherwise
		//return null.
		//TODO: Return a full parse.
		cur = cols.get(cols.size()-1);
		Production finished = new Production(getProds("ROOT").get(0));
		finished.dot = finished.right.length;
		for(int i=0;i<cur.size();i++)
		{
			p = cur.get(i);
			if(p.equals(finished))
			{
				return p;
			}
		}
		return null;
	}

	/**this prints the table in a human-readable fashion.
	 * format is one column at a time, lists the word in the sentence
	 * and then the productions for that column.
	 * @param cols The columns of the table
	 * @param sent the sentence
	 */
	private final void printTable(Vector<Vector<Production>> cols,String sent[])
	{
		Vector<Production> col;
		//print one column at a time
		for(int i=0;i<cols.size();i++)
		{
			col = cols.get(i);
			//sort the columns by
			if(i>0)
			{
				System.out.println("\nColumn "+i+": "+sent[i-1]+"\n------------------------");
			}else{
				System.out.println("\nColumn "+i+": ROOT\n------------------------");
			}

			for(int j=0;j<col.size();j++)
			{
				System.out.println((col.get(j)).toString());
			}

		}
    }

	//this adds a production p to the vector v of production indices
	//it also checks for duplicate indices, and skips those
	private final void addProd(Vector<Production> v, Production p)
	{
		//check for duplicates
		for(int i=0;i<v.size();i++)
			if((v.get(i)).equals(p))
				return;
		v.add(p);
	}

	//This runs through the columns and returns all the fully parsed productions
	//i.e. those with little dots at the very end.
	private final Vector<Production> getFinalProds(Vector<Vector<Production>> cols)
	{
		Vector<Production> cur;
		Vector<Production> prods = new Vector<Production>();	prods.clear();
		Production p;
		for(int i=0; i<cols.size(); i++)
		{
			cur = cols.get(i);
			for(int j=0;j<cur.size();j++)
			{
				p = cur.get(j);
				if(p.right.length == p.dot)
				{
					if(p.left.compareTo("ROOT")!=0)
					{
						prods.add(p);
					}
				}
			}
		}
		//convert it to an array for returning
		return prods;
	}

	//this returns true if a string is in the grammar, false otherwise
	//it's not exactly "comprehensive"... mostly it'll just see if all
	//the tokens in the sentence are terminals.
	private final boolean inGrammar(String s)
	{
		boolean found=false;
		Production p;
		for(int i=0;i<productions.size();i++)
		{
			p = productions.get(i);
			for(int j=0;j<p.right.length;j++)
				if(p.right[j].indexOf(s)!=-1)
					found = true;
			//we can't have a string equal to a non-terminal
			if(p.left.compareTo(s)==0)
			{
				System.out.println("String contains a non-terminal - cannot parse");
				return false;
			}
		}
		return found;
	}

	//this returns a vector of productions with a left side matching the
	//argument; happy string comparing.
	private final Vector<Production> getProds(String left)
	{
		//we store it in a vector for safekeeping
		Vector<Production> prods = new Vector<Production>();	prods.clear();
		Production p;
		for(int i=0;i<productions.size();i++)
		{
			p = productions.get(i);
			if(p.left.compareTo(left)==0)
				prods.add(p);
		}
		//convert it to an array for returning
		return prods;
	}

	//this checks if the given string[] has a parse tree with this grammar
	//
	public final boolean canParse(String sent[])
	{
		//check if all symbols are in the grammar
		for(int i=0;i<sent.length;i++)
			if(!inGrammar(sent[i]))
				return false;
		return true;
	}

	//this prints out the grammar
	public void print()
	{
		System.out.println(this.toString());
	}

	//what does every toString function do?
	public String toString()
	{
		String ret = "";
		for(int i=0;i<productions.size();i++)
			ret = ret + (productions.get(i)).toString() + "\n";
		return ret;
	}
}
