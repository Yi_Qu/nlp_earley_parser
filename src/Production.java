/**This implements a production in a pcfg grammar
 *
 * @author Paul Chase: chaspau@iit.edu
 * @version 1.0
 *
 */

import java.io.*;
import java.text.*;
import java.util.*;
import java.util.regex.*;

public class Production
{
	float probability;
	String left;
	String right[];
	int dot;
	int start;

	//Production does double duty as a parse tree; this is for that.
	//therefore, has same number of children as right[], one for
	//each; if there is no child there, null is stored instead.
	Production children[];
	//parent is for the linking as well.
	Production parent;

    Production nextProd;

	/**Simple constructor, assumes no children, initializes everybody.*/
	Production()
	{
		probability=0.0f;
		left = "";
		right = null;
		dot = 0;
		start = 0;
		children = null;
		parent = null;

        nextProd = null;
	}

	/**Constructs a production with n right productions.*/
	Production(int n)
	{
		this();
		right = new String[n];
		children = new Production[n];
		for(int i=0;i<n;i++)
		{
			right[i] = null;
			children[i] = null;
		}
        nextProd = null;
	}

	/**Constructs a production with the given right hand side.*/
	Production(String[] rhs)
	{
		this(rhs.length);
		right = rhs;
	}

	/**Copy constructor.*/
	Production(Production p)
	{
		probability = p.probability;
		left = p.left;
		right = p.right;
		dot = p.dot;
		start = p.start;
		children = p.children;
        nextProd = p.nextProd;
	}

	/**This creates a child of the production given its index.
	 * This adds the child to the production and sets the parent for
	 * the newly created child production.
	 *
	 * @param n the index on the right hand side where the child attaches
	 * @return The newly created child
	 */
	public final Production spawn(int n)
	{
		Production p = new Production();
		p.parent = this;
		children[n] = p;
		return p;
	}

	/**This creates a child of the production given its index.
         * This adds the child to the production and sets the parent for
         * the newly created child production.  The new child production
	     * will be a copy of the production input as a parameter.
         *
         * @param n the index on the right hand side where the child attaches
	     * @param prod the production to copy the child from
         * @return The newly created child
         */
        public final Production spawn(int n, Production prod)
        {
                Production p = new Production(prod);
                p.parent = this;
                children[n] = p;
                return p;
        }

	/**This returns true if the given production matches this one.
	 *
	 * The comparison checks for identical productions only, down to the
	 * placement of the dot.
	 *
	 * @param p The production to compare to.
	 */
	public final boolean equals(Production p)
	{
		if(left != p.left || right.length != p.right.length || dot != p.dot || start != p.start)
			return false;
		for(int i=0;i<right.length;i++)
			if(right[i] != p.right[i])
				return false;
		return true;
	}

	/**Easy print.
	 */
	public void print()
	{
		System.out.println(this.toString());
	}

	/**Standard toString human-readable output.
	 * Format:
	 * startpos  left-- right1 . right2
	 * with the dot moving about accordingly.
	 */
	public String toString()
	{
		String ret = start+"\t"+left+"->";
                for(int i=0;i<right.length;i++)
                {
                        if(i==dot)
                                ret = ret + "\t.";
                        ret = ret + "\t" + right[i];
                }
                if(dot == right.length)
                        ret = ret + "\t.";
                return ret;
	}

	/**This prints a parse, a chain of productions.
	 * TODO: Write this function!
	 */
	public void recursivePrint()
	{
	    Production p = this;
        String toPrint = new String();//Passing it through the recursive function
        if(p.probability == 0.0){
            System.out.println("\nNo Probability Provided!!\n");
        } else {
            System.out.println("\nProbability Implemented!!\n");
        }
        toPrint = String.format("%s", printHelper(p, toPrint));
        toPrint = toPrint.substring(0, toPrint.length()-1);
        System.out.println(toPrint + "\n");
    }

    public String printHelper(Production p, String toPrint){
        if(p != null){
            if(p.children == null){
                toPrint = String.format("%s %s ]", toPrint, p.right[0]);//reach bottom
            } else {
                for(int i=0; i<p.children.length; i++){
                    if(p.children[i] != null){
                        toPrint = String.format("%s %s[", toPrint, p.children[i].left);//add left
                        toPrint = String.format("%s", printHelper(p.children[i], toPrint));//recursive print rights
                        if(p.children[i].nextProd != null){
                            toPrint = toPrint.substring(0, toPrint.length()-1);
                            toPrint = String.format("%s || ", toPrint);//found parallel branches
                            toPrint = String.format("%s", printHelper(p.children[i].nextProd, toPrint));//recursive print parallel branches
                        }
                    }
                }
                toPrint = String.format("%s ]", toPrint);//reach bottom
            }
        }
        return toPrint;
    }
}
